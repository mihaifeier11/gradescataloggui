package main.newGUI;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import main.java.Domain.Nota;
import main.java.Domain.Student;
import main.java.Domain.Tema;
import main.java.Exceptions.ValidationException;
import main.java.Service.Service;
import main.java.utils.ChangeEventType;
import main.java.utils.Observer;
import main.java.utils.StudentChangeEvent;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public class FiltrareController implements Observer<StudentChangeEvent> {

    private ObservableList<Integer> grupe = FXCollections.observableArrayList();
    private ObservableList<String> teme = FXCollections.observableArrayList();
    private ObservableList<String> studentiGrupe = FXCollections.observableArrayList();
    private ObservableList<String> tempStudentiGrupe = FXCollections.observableArrayList();
    private ObservableList<Filtrare> filtrareData = FXCollections.observableArrayList();
    private Service service = new Service();
    private TableView<Pair<String, String>> table = new TableView<>();
    private TableView<Filtrare> tableFiltrareData = new TableView<>();
    private ObservableList<Pair<String, String>> filtrare = FXCollections.observableArrayList();

    @FXML
    TextField sfarsitDataComboBox, startDataComboBox;

    @FXML
    ComboBox<Integer> grupaFiltrareComboBox, grupaAllStudentsComboBox;
    @FXML
    ComboBox<String> filtrareTemeComboBox, filtrareStudentComboBox, temaFiltrareGrupaComboBox;

    @FXML
    Button filtrareTemeButton, filtrareStudentButton, filtrareGrupaTemaButton, filtrareDataButton;

    @FXML
    public void initialize() {
        setTemaComboBox();

        filtrareTeme();

        setStudentComboBox();

        filtrareGrupa();

        filtrareGrupaTema();

        filtrareDataButton.setOnAction(e -> {
            filtrareData.clear();
            service.getAllNote().forEach(nota -> {
                System.out.println(nota.toStringC());
                if (nota.getData() > Integer.parseInt(startDataComboBox.getText()) && nota.getData() < Integer.parseInt(sfarsitDataComboBox.getText())) {
                    filtrareData.add(new Filtrare(nota.getStudent().getNume(), nota.getTema().getId(), nota.getValoare()));
                }
            });


            System.out.println(filtrareData);
            tableFiltrareData.getColumns().clear();
            tableFiltrareData.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            tableFiltrareData.setItems(filtrareData);
            Stage stage = new Stage();
            Scene scene = new Scene(new AnchorPane());
            stage.setTitle("Filtrare Teme");

            TableColumn nume = new TableColumn("Nume");
            nume.setResizable(true);
            nume.setCellValueFactory(
                    new PropertyValueFactory<Filtrare, String>("numeStudent"));

            TableColumn id = new TableColumn("Tema");
            id.setResizable(true);
            id.setCellValueFactory(
                    new PropertyValueFactory<Filtrare, String>("idTema"));

            TableColumn n = new TableColumn("Nota");
            n.setResizable(true);
            n.setCellValueFactory(
                    new PropertyValueFactory<Filtrare, Integer>("nota"));

            tableFiltrareData.getColumns().addAll(nume, id, n);

            ((AnchorPane) scene.getRoot()).getChildren().addAll(tableFiltrareData);

            stage.setScene(scene);
            stage.show();
        });

    }

    public void filtrareGrupaTema() {
        Integer currentWeek = service.setCurrentWeek();
        String temaCurenta = "";

        teme.clear();
        for (Tema hw : service.getAllTeme()) {
            teme.add(hw.getId());
            if (hw.getDeadline() == currentWeek) {
                temaCurenta = hw.getId();
            }

        }
        temaFiltrareGrupaComboBox.setItems(teme);
        temaFiltrareGrupaComboBox.getSelectionModel().select(temaCurenta);

        grupe.clear();
        Set<Integer> set = new HashSet();
        service.getAllStudents().forEach(e -> set.add(e.getGrupa()));
        set.forEach(e -> grupe.add(e));

        grupaFiltrareComboBox.setItems(grupe);
        filtrare.clear();
        filtrareGrupaTemaButton.setOnAction(e -> {
            System.out.println("Here");
            service.getAllNote().forEach(nota -> {
                if (nota.getStudent().getGrupa() == grupaFiltrareComboBox.getValue() && nota.getTema().getId().equals(temaFiltrareGrupaComboBox.getValue())) {
                    filtrare.add(new Pair(nota.getStudent().getNume(), Integer.toString(nota.getValoare())));

                }
            });
            System.out.println(filtrare);
            table.getColumns().clear();
            table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            table.setItems(filtrare);
            Stage stage = new Stage();
            Scene scene = new Scene(new AnchorPane());
            stage.setTitle("Filtrare Teme");

            TableColumn nume = new TableColumn("Nume");
            nume.setResizable(true);
            nume.setCellValueFactory(
                    new PropertyValueFactory<Pair, String>("key"));

            TableColumn nota = new TableColumn("Nota");
            nota.setResizable(true);
            nota.setCellValueFactory(
                    new PropertyValueFactory<Pair, String>("value"));

            table.getColumns().addAll(nume, nota);

            ((AnchorPane) scene.getRoot()).getChildren().addAll(table);

            stage.setScene(scene);
            stage.show();
        });
    }

    public void filtrareGrupa() {
        filtrareStudentButton.setOnAction(e -> {
            filtrare.clear();
            table.getColumns().clear();
            table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);


            service.getAllNote().forEach(nota -> {
                if (nota.getStudent().getId().equals(filtrareStudentComboBox.getValue().split(" # ")[1])) {
                    Pair pair = new Pair(nota.getTema().getId(), nota.getValoare());
                    filtrare.add(pair);
                }
            });
            table.setItems(filtrare);
            Stage stage = new Stage();
            Scene scene = new Scene(new AnchorPane());
            stage.setTitle("Filtrare Student");

            TableColumn nume = new TableColumn("Tema");
            nume.setResizable(true);
            nume.setCellValueFactory(
                    new PropertyValueFactory<Pair, String>("key"));

            TableColumn nota = new TableColumn("Nota");
            nota.setResizable(true);
            nota.setCellValueFactory(
                    new PropertyValueFactory<Pair, String>("value"));

            table.getColumns().addAll(nume, nota);

            ((AnchorPane) scene.getRoot()).getChildren().addAll(table);

            stage.setScene(scene);
            stage.show();
        });
    }

    public void filtrareTeme() {
        filtrareTemeButton.setOnAction(e -> {
            filtrare.clear();
            table.getColumns().clear();
            table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

            service.getAllNote().forEach(nota -> {
                System.out.println(nota.getTema().getId());
                if (nota.getTema().getId().equals(filtrareTemeComboBox.getValue())) {
                    Pair pair = new Pair(nota.getStudent().getNume(), Integer.toString(nota.getValoare()));
                    filtrare.add(pair);
                }
            });
            table.setItems(filtrare);
            Stage stage = new Stage();
            Scene scene = new Scene(new AnchorPane());
            stage.setTitle("Filtrare Teme");

            TableColumn nume = new TableColumn("Nume");
            nume.setResizable(true);
            nume.setCellValueFactory(
                    new PropertyValueFactory<Pair, String>("key"));

            TableColumn nota = new TableColumn("Nota");
            nota.setResizable(true);
            nota.setCellValueFactory(
                    new PropertyValueFactory<Pair, String>("value"));

            table.getColumns().addAll(nume, nota);

            ((AnchorPane) scene.getRoot()).getChildren().addAll(table);

            stage.setScene(scene);
            stage.show();
        });
    }


    public void setStudentComboBox() {
        service.getAllStudents().forEach(e -> {
            studentiGrupe.add(e.getNume() + " # " + e.getId());
        });
        filtrareStudentComboBox.setEditable(true);

        filtrareStudentComboBox.setItems(studentiGrupe);

        filtrareStudentComboBox.getEditor().textProperty().addListener(((observable, oldValue, newValue) -> {
            tempStudentiGrupe.clear();
            studentiGrupe.forEach(e -> {
                if (e.toLowerCase().contains(newValue.toLowerCase())) {
                    tempStudentiGrupe.add(e);
                }
            });
            filtrareStudentComboBox.setItems(tempStudentiGrupe);
        }));
    }

//    public void setGrupaComboBox() {
//        Set<Integer> set = new HashSet();
//        service.getAllStudents().forEach(e -> set.add(e.getGrupa()));
//        set.forEach(e -> grupe.add(e));
//
//        grupaComboBox.setItems(grupe);
//
//        grupaComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
//            studentiGrupe.clear();
//            service.getAllStudents().forEach(e -> {
//                if (newValue == e.getGrupa()) {
//                    studentiGrupe.add(e.getNume() + " # " + e.getId());
//                }
//            });
//
//            studentComboBox.setItems(studentiGrupe);
//        });
//    }

    public void setTemaComboBox() {
        Integer currentWeek = service.setCurrentWeek();
        String temaCurenta = "";

        for (Tema hw : service.getAllTeme()) {
            teme.add(hw.getId());
            if (hw.getDeadline() == currentWeek) {
                temaCurenta = hw.getId();
            }

        }
        filtrareTemeComboBox.setItems(teme);
        filtrareTemeComboBox.getSelectionModel().select(temaCurenta);
    }


    private void showMessage(Alert.AlertType type, String header, String text) {
        Alert message = new Alert(type);
        message.setHeaderText(header);
        message.setContentText(text);
        message.showAndWait();
    }

    @Override
    public void update(StudentChangeEvent studentChangeEvent) {
//        model.clear();
//        for (Nota n : service.getAllNote()) {
//            model.add(n);
//        }
//
//        //tableViewStudents.setItems(model);
    }


}






