package main.newGUI;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import main.java.Domain.Nota;
import main.java.Domain.Student;
import main.java.Domain.Tema;
import main.java.Exceptions.ValidationException;
import main.java.Service.Service;
import main.java.utils.ChangeEventType;
import main.java.utils.Observer;
import main.java.utils.StudentChangeEvent;

import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public class NoteController implements Observer<StudentChangeEvent> {

    private ObservableList<Integer> grupe = FXCollections.observableArrayList();
    private ObservableList<String> teme = FXCollections.observableArrayList();
    private ObservableList<String> studentiGrupe = FXCollections.observableArrayList();
    private ObservableList<String> tempStudentiGrupe = FXCollections.observableArrayList();
    private Service service = new Service();

    @FXML
    CheckBox motivatCheckBox;

    @FXML
    Button buttonIesire, buttonSalveaza, filtrariMainButton;

    @FXML
    TextArea feedbackTextArea;

    @FXML
    TextField notaTextfield;

    @FXML
    ComboBox<String> studentComboBox, temaComboBox;
    @FXML
    ComboBox<Integer> grupaComboBox;

    @FXML
    public void initialize() {
        setTemaComboBox();

        setGrupaComboBox();

        setStudentComboBox();


        salveaza();

        filtrariMainButton.setOnAction(e -> {
            Stage primaryStage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("filtrari.fxml"));
            AnchorPane root = null;
            try {
                root = loader.load();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            primaryStage.setScene(new Scene(root));
            primaryStage.setTitle("Filtrari");
            primaryStage.show();
        });

    }

    public void salveaza() {
        buttonSalveaza.setOnAction(e -> {
            String tema = temaComboBox.getValue();
            String student = studentComboBox.getValue();
            Integer nota = Integer.parseInt(notaTextfield.getText());
            String feedback = feedbackTextArea.getText();
            Boolean motivat = motivatCheckBox.isSelected();

            String idStudent = student.split(" # ")[1];
            Tema hw = (Tema) service.getTema(tema);
            Student st = (Student) service.getStudent(idStudent);
            System.out.println(idStudent);
            Nota n = new Nota(new Pair(tema, idStudent), st, hw, nota, service.setCurrentWeek(), hw.getDeadline(), feedback);

            Alert message = new Alert(Alert.AlertType.CONFIRMATION);
            message.setHeaderText("Doriti sa adaugati nota?");
            message.setContentText(n.toStringC());

            Optional<ButtonType> button = message.showAndWait();
            if (button.get() == ButtonType.OK) {
                try {
                    if (motivat) {
                        service.addNota(idStudent, tema, nota, feedback, motivat);
                    } else {
                        service.addNota(idStudent, tema, nota, feedback);
                    }

                } catch (ValidationException e1) {
                    showMessage(Alert.AlertType.ERROR, "Eroare!", "Date incorecte!");
                }
            }

        });
    }

    public void setStudentComboBox() {
        studentComboBox.setEditable(true);

        studentComboBox.getEditor().textProperty().addListener(((observable, oldValue, newValue) -> {
            tempStudentiGrupe.clear();
            studentiGrupe.forEach(e -> {
                if (e.toLowerCase().contains(newValue.toLowerCase())) {
                    tempStudentiGrupe.add(e);
                }
            });
            studentComboBox.setItems(tempStudentiGrupe);
        }));
    }

    public void setGrupaComboBox() {
        Set<Integer> set = new HashSet();
        service.getAllStudents().forEach(e -> set.add(e.getGrupa()));
        set.forEach(e -> grupe.add(e));

        grupaComboBox.setItems(grupe);

        grupaComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            studentiGrupe.clear();
            service.getAllStudents().forEach(e -> {
                if (newValue == e.getGrupa()) {
                    studentiGrupe.add(e.getNume() + " # " + e.getId());
                }
            });

            studentComboBox.setItems(studentiGrupe);
        });
    }

    public void setTemaComboBox() {
        Integer currentWeek = service.setCurrentWeek();
        String temaCurenta = "";

        for (Tema hw : service.getAllTeme()) {
            teme.add(hw.getId());
            if (hw.getDeadline() == currentWeek) {
                temaCurenta = hw.getId();
            }

        }
        temaComboBox.setItems(teme);
        temaComboBox.getSelectionModel().select(temaCurenta);
    }


    private void showMessage(Alert.AlertType type, String header, String text) {
        Alert message = new Alert(type);
        message.setHeaderText(header);
        message.setContentText(text);
        message.showAndWait();
    }

    @Override
    public void update(StudentChangeEvent studentChangeEvent) {
//        model.clear();
//        for (Nota n : service.getAllNote()) {
//            model.add(n);
//        }
//
//        //tableViewStudents.setItems(model);
    }


}






