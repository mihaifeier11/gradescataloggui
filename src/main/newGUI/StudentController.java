package main.newGUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import main.java.Domain.Student;
import main.java.Exceptions.ValidationException;
import main.java.Service.Service;
import main.java.utils.ChangeEventType;
import main.java.utils.Observer;
import main.java.utils.StudentChangeEvent;


public class StudentController implements Observer<StudentChangeEvent> {

    private ObservableList<Student> model = FXCollections.observableArrayList();
    private Service service = new Service();
    @FXML
    TableColumn<Student, Double> emailStudentTableColumn;
    @FXML
    TableColumn<Student, Double> cadruIndrumatorStudentTableColumn;
    @FXML
    TableColumn<Student, Double> grupaStudentTableColumn;
    @FXML
    TableColumn<Student, String> numeStudentTableColumn;
    @FXML
    TableColumn<Student, String> idStudentTableColumn;
    @FXML
    TableView<Student> tableViewStudents;
    @FXML
    Button buttonAdaugaStudent, buttonStergeStudent, buttonModificaStudent, buttonGolire;
    @FXML
    TextField textfieldIdStudent, textfieldNumeStudent, textfieldGrupaStudent, textfieldEmailStudent, textfieldCadruIndrumatorStudent;

    public void asd(String text) {
        textfieldIdStudent.setText(text);
    }

    @FXML
    public void initialize() {
        idStudentTableColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        numeStudentTableColumn.setCellValueFactory(new PropertyValueFactory<>("nume"));
        grupaStudentTableColumn.setCellValueFactory(new PropertyValueFactory<>("grupa"));
        emailStudentTableColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        cadruIndrumatorStudentTableColumn.setCellValueFactory(new PropertyValueFactory<>("cadruIndrumator"));

        service.addObserver(this);

        update(new StudentChangeEvent(ChangeEventType.UPDATE, null, null));


        Student st = tableViewStudents.getItems().get(0);

        updateTextfields(st);
        updateSelectedItem();

        adaugaStudentButton();
        stergeStudentButton();
        modificaStudentButton();
        golireButton();

    }

    private void golireButton() {
        buttonGolire.setOnAction(e -> clearTextfields());
    }

    private void updateTextfields(Student st) {
        textfieldIdStudent.setText(st.getId());
        textfieldNumeStudent.setText(st.getNume());
        textfieldGrupaStudent.setText(Integer.toString(st.getGrupa()));
        textfieldEmailStudent.setText(st.getEmail());
        textfieldCadruIndrumatorStudent.setText(st.getCadruIndrumator());
    }

    private void clearTextfields() {
        textfieldIdStudent.clear();
        textfieldNumeStudent.clear();
        textfieldGrupaStudent.clear();
        textfieldEmailStudent.clear();
        textfieldCadruIndrumatorStudent.clear();

    }

    private void updateSelectedItem() {
        tableViewStudents.setOnMouseClicked(e -> {
            Student st = tableViewStudents.getSelectionModel().getSelectedItem();

            textfieldIdStudent.setText(st.getId());
            textfieldNumeStudent.setText(st.getNume());
            textfieldGrupaStudent.setText(Integer.toString(st.getGrupa()));
            textfieldEmailStudent.setText(st.getEmail());
            textfieldCadruIndrumatorStudent.setText(st.getCadruIndrumator());
        });

    }

    private void modificaStudentButton() {
        buttonModificaStudent.setOnAction(e -> {
            String id = textfieldIdStudent.getText();
            String nume = textfieldNumeStudent.getText();
            String grupa = textfieldGrupaStudent.getText();
            String email = textfieldEmailStudent.getText();
            String cadruIndrumator = textfieldCadruIndrumatorStudent.getText();
            Student st = new Student(id, nume, Integer.parseInt(grupa), email, cadruIndrumator);
            try {
                Object ob = service.updateStudent(st);
                if (ob != null) {
                    showMessage(Alert.AlertType.ERROR, "Eroare!", "Studentul cu acest id nu exista!");
                } else {
                    showMessage(Alert.AlertType.INFORMATION, "Succes!", "Studentul a fost modificat cu succes!");
                }
            } catch (ValidationException e1) {
                showMessage(Alert.AlertType.ERROR, "Eroare!", "Date incorecte!");
            }
        });
    }

    private void stergeStudentButton() {
        buttonStergeStudent.setOnAction(e -> {
            String id = textfieldIdStudent.getText();
            Object ob = service.deleteStudent(id);
            if (ob == null) {
                showMessage(Alert.AlertType.ERROR, "Eroare!", "Studentul cu acest numar matricol nu exista");
            } else {
                showMessage(Alert.AlertType.INFORMATION, "Succes!", "Studentul " + ((Student) ob).getNume() + " a fost sters cu succes!");
            }
        });
    }

    private void adaugaStudentButton() {
        buttonAdaugaStudent.setOnAction(e -> {
            String id = textfieldIdStudent.getText();
            String nume = textfieldNumeStudent.getText();
            String grupa = textfieldGrupaStudent.getText();
            String email = textfieldEmailStudent.getText();
            String cadruIndrumator = textfieldCadruIndrumatorStudent.getText();
            try {
                Object ob = service.addStudent(id, nume, Integer.parseInt(grupa), email, cadruIndrumator);
                if (ob != null) {
                    showMessage(Alert.AlertType.ERROR, "Eroare!", "Studentul cu acest numar matricol exista deja!");
                } else {
                    showMessage(Alert.AlertType.INFORMATION, "Succes!", "Studentul " + nume + " a fost adaugat cu succes!");
                }
            } catch (Exception e1) {
                showMessage(Alert.AlertType.ERROR, "Eroare!", "Datele introduse sunt incorecte!");
            }
        });
    }

    private void showMessage(Alert.AlertType type, String header, String text) {
        Alert message = new Alert(type);
        message.setHeaderText(header);
        message.setContentText(text);
        message.showAndWait();
    }

    @Override
    public void update(StudentChangeEvent studentChangeEvent) {
        model.clear();
        for (Student st : service.getAllStudents()) {
            model.add(st);
        }

        tableViewStudents.setItems(model);
    }


}
