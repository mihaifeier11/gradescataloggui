package main.newGUI;


public class Filtrare {


    String numeStudent;
    String idTema;
    Integer nota;

    public Filtrare(String numeStudent, String idTema, Integer nota) {
        this.numeStudent = numeStudent;
        this.idTema = idTema;
        this.nota = nota;
    }

    public String getNumeStudent() {
        return numeStudent;
    }

    public void setNumeStudent(String numeStudent) {
        this.numeStudent = numeStudent;
    }

    public String getIdTema() {
        return idTema;
    }

    public void setIdTema(String idTema) {
        this.idTema = idTema;
    }

    public Integer getNota() {
        return nota;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }


}