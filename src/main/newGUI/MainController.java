package main.newGUI;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import main.java.Domain.Nota;
import main.java.Domain.Student;
import main.java.Domain.Tema;
import main.java.Exceptions.ValidationException;
import main.java.Service.Service;
import main.java.utils.ChangeEventType;
import main.java.utils.Observer;
import main.java.utils.StudentChangeEvent;

import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public class MainController implements Observer<StudentChangeEvent> {

    private ObservableList<Integer> grupe = FXCollections.observableArrayList();
    private ObservableList<String> teme = FXCollections.observableArrayList();
    private ObservableList<String> studentiGrupe = FXCollections.observableArrayList();
    private ObservableList<String> tempStudentiGrupe = FXCollections.observableArrayList();
    private ObservableList<Filtrare> filtrareData = FXCollections.observableArrayList();
    private Service service = new Service();
    private TableView<Pair<String, String>> table = new TableView<>();
    private TableView<Filtrare> tableFiltrareData = new TableView<>();
    private ObservableList<Pair<String, String>> filtrare = FXCollections.observableArrayList();

    @FXML
    Button noteButton, studentiButton, temeButton;

    @FXML
    public void initialize() {
        studentiButton.setOnAction(e -> {
            Stage primaryStage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("student.fxml"));
            AnchorPane root = null;
            try {
                root = loader.load();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            StudentController ctrl = loader.getController();
            ctrl.asd("asd");
            //NoteController ctrl=loader.getController();


            primaryStage.setScene(new Scene(root));
            primaryStage.setTitle("Catalog Studenti");
            primaryStage.show();
        });

        noteButton.setOnAction(e -> {
            Stage secondaryStage = new Stage();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("note.fxml"));
            AnchorPane root = null;
            try {
                root = loader.load();
            } catch (IOException e1) {
                e1.printStackTrace();
            }


            secondaryStage.setScene(new Scene(root));
            secondaryStage.setTitle("Note Studenti");
            secondaryStage.show();

        });

    }


    @Override
    public void update(StudentChangeEvent studentChangeEvent) {

    }
}






