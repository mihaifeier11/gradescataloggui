package main.java.Service;

import main.java.Validator.NotaValidator;
import main.java.Validator.StrategyValidator;
import main.java.Validator.VEValidator;
import main.java.Domain.Nota;
import main.java.Domain.Strategy;
import main.java.Domain.Student;
import main.java.Domain.Tema;
import main.java.Exceptions.ValidationException;
import main.java.Repository.CrudRepository;
import main.java.Repository.NotaFileRepository;
import main.java.Repository.StudentFileRepository;
import main.java.Repository.TemaFileRepository;
import javafx.util.Pair;
import main.java.utils.ChangeEventType;
import main.java.utils.Observable;
import main.java.utils.Observer;
import main.java.utils.StudentChangeEvent;
import main.newGUI.NoteController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Service implements Observable<StudentChangeEvent> {
    CrudRepository<String, Student> studentRepo = new StudentFileRepository();
    CrudRepository<String, Tema> temaRepo = new TemaFileRepository();
    CrudRepository<String, Nota> notaRepo = new NotaFileRepository();
    Integer currentWeek = 0;

    VEValidator studentValidator = new StrategyValidator().validator(Strategy.Student);
    VEValidator temaValidator = new StrategyValidator().validator(Strategy.Tema);
    NotaValidator notaValidator = new NotaValidator();


    public Service() {

    }

    public Iterable<Nota> getAllNote() {
        return notaRepo.findAll();
    }

    public int setCurrentWeek() {
        LocalDate localDate;
        localDate = LocalDate.now();
        int date = localDate.getDayOfYear();
        date = (date - 1) / 7 + 1 - 39;
        this.currentWeek = date;
        return this.currentWeek;
    }

    public Object addStudent(String id, String nume, Integer grupa, String email, String cadruIndrumator) throws ValidationException {
        Student student = new Student(id, nume, grupa, email, cadruIndrumator);
        studentValidator.validate(student);
        Student st = studentRepo.save(student);
        if (st == null) {
            notifyObservers(new StudentChangeEvent(ChangeEventType.ADD, st));
        }
        return st;

    }

    public Object getStudent(String id) {
        return studentRepo.findOne(id);
    }

    public Iterable<Student> getAllStudents() {
        return studentRepo.findAll();
    }

    public Object updateStudent(Student student) throws ValidationException {
        studentValidator.validate(student);
        Student st = studentRepo.update(student);
        if (st == null) {
            notifyObservers(new StudentChangeEvent(ChangeEventType.ADD, st));
        }
        return st;
    }

    public Object deleteStudent(String id) {
        Student st = studentRepo.delete(id);
        if (st != null) {
            notifyObservers(new StudentChangeEvent(ChangeEventType.DELETE, st));
        }
        return st;
    }

    public Object addTema(String id, String descriere, Integer deadline, Integer dateReceived) throws ValidationException {
        Tema tema = new Tema(id, descriere, deadline, dateReceived);
        temaValidator.validate(tema);
        return temaRepo.save(tema);
    }

    public Object setDeadline(String id, Integer deadline) throws ValidationException {
        Tema tema = temaRepo.findOne(id);
        tema.setDeadline(deadline);
        temaValidator.validate(tema);
        return temaRepo.update(tema);
    }

    public Object getTema(String id) {
        return temaRepo.findOne(id);
    }

    public Iterable<Tema> getAllTeme() {
        return temaRepo.findAll();
    }

    public Object addNota(String idStudent, String idTema, Integer valoare, String feedback) throws ValidationException {
        Student student = studentRepo.findOne(idStudent);
        Tema tema = temaRepo.findOne(idTema);
        Integer deadline = tema.getDeadline();
        Nota nota = new Nota(new Pair<String, String>(idStudent, idTema), student, tema, valoare, this.currentWeek, deadline, feedback);


        if (student != null && tema != null) {
            notaValidator.validateNota(studentRepo, temaRepo, nota);

            Integer penalizare = (nota.getData() - tema.getDeadline()) * 2;

            Penalizare pf = p -> {
                return penalizare < 0 ? 0 : penalizare;
            };

            if (penalizare > 0) {
                nota.setFeedback(feedback = feedback + "\n" + "NOTA A FOST DIMINUATĂ CU " + Integer.toString(penalizare) + " PUNCTE DATORITĂ ÎNTÂRZIERILOR!");
                System.out.println(nota.getFeedback());
            }


            nota.setValoare(nota.getValoare() - pf.func(penalizare));

            return notaRepo.save(nota);
        } else {
            System.out.println("Id-ul studentului sau al temei este incorect.");
        }
        return nota;
    }

    public Object addNota(String idStudent, String idTema, Integer valoare, String feedback, boolean motivare) throws ValidationException {
        Student student = studentRepo.findOne(idStudent);
        Tema tema = temaRepo.findOne(idTema);
        Integer deadline = tema.getDeadline();
        Nota nota = new Nota(new Pair<String, String>(idStudent, idTema), student, tema, valoare, this.currentWeek, deadline, feedback);


        if (student != null && tema != null) {
            notaValidator.validateNota(studentRepo, temaRepo, nota);

            return notaRepo.save(nota);
        } else {
            System.out.println("Id-ul studentului sau al temei este incorect.");
        }
        return nota;
    }


    private List<Observer<StudentChangeEvent>> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer<StudentChangeEvent> e) {
        observers.add(e);

    }

    @Override
    public void removeObserver(Observer<StudentChangeEvent> e) {
        //observers.remove(e);
    }

    @Override
    public void notifyObservers(StudentChangeEvent t) {
        observers.stream().forEach(x -> x.update(t));
    }
}

interface Penalizare {
    int func(Integer n);
}
