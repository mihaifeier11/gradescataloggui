package main.java.Validator;

public interface IAEValidator<E> {
    /**
     * @param entity
     * @throws IllegalArgumentException - if the given entity is null
     */
    void validate(E entity) throws IllegalArgumentException;
}
