package main.java.Validator;

import main.java.Domain.Nota;
import main.java.Domain.Student;
import main.java.Domain.Tema;
import main.java.Exceptions.ValidationException;
import main.java.Repository.CrudRepository;

public class NotaValidator implements VEValidator<Nota> {
    @Override
    public void validate(Nota entity) {

    }

    public void validateNota(CrudRepository studentRepo, CrudRepository temaRepo, Nota nota) throws ValidationException {
        Tema tema = nota.getTema();
        Student student = nota.getStudent();

        if (studentRepo.findOne(student.getId()) == null) {
            throw new ValidationException("Studentul nu exista");
        }

        if (temaRepo.findOne(tema.getId()) == null) {
            throw new ValidationException("Tema  nu exista");
        }
        if (nota.getData() > tema.getDeadline() + 2) {
            throw new ValidationException("Tema nu mai poate fi predata, deadline-ul a fost depasit cu mai mult de 2 saptamani");
        }
    }
}
