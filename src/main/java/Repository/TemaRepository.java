package main.java.Repository;


import main.java.Validator.IAEValidator;
import main.java.Validator.NullValidator;
import main.java.Validator.StrategyValidator;
import main.java.Validator.VEValidator;
import main.java.Domain.Strategy;
import main.java.Domain.Tema;
import main.java.Exceptions.ValidationException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TemaRepository implements CrudRepository<String, Tema> {
    private Map<String, Tema> repository = new HashMap();
    private IAEValidator nullValidator = new NullValidator();
    private VEValidator temaValidator = new StrategyValidator().validator(Strategy.Tema);


    @Override
    public Tema findOne(String id) throws IllegalArgumentException {
        nullValidator.validate(id);
        return repository.get(id);
    }

    @Override
    public Collection<Tema> findAll() {
        return repository.values();
    }

    @Override
    public Tema save(Tema tema) throws ValidationException {
        nullValidator.validate(tema);
        temaValidator.validate(tema);

        String id = tema.getId();
        if (repository.get(id) == null) {
            repository.put(id, tema);
            return null;
        }
        return tema;
    }

    @Override
    public Tema delete(String id) {
        nullValidator.validate(id);
        if (repository.get(id) == null) {
            return null;
        } else {
            return repository.remove(id);
        }
    }

    @Override
    public Tema update(Tema tema) throws ValidationException {
        nullValidator.validate(tema);
        temaValidator.validate(tema);

        Object id = tema.getId();
        Tema ob = repository.get(id);
        if (ob != null) {
            ob.setDeadline(tema.getDeadline());
            return null;
        }
        return tema;
    }
}

