package main.java.Repository;

import main.java.Validator.IAEValidator;
import main.java.Validator.NullValidator;
import main.java.Validator.StrategyValidator;
import main.java.Validator.VEValidator;
import main.java.Domain.Strategy;
import main.java.Domain.Student;
import main.java.Exceptions.ValidationException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class StudentRepository implements CrudRepository<String, Student> {

    Map<String, Student> repository = new HashMap();
    IAEValidator nullValidator = new NullValidator();
    VEValidator studentValidator = new StrategyValidator().validator(Strategy.Student);

    @Override
    public Student findOne(String id) throws IllegalArgumentException {
        nullValidator.validate(id);
        return repository.get(id);
    }


    @Override
    public Collection<Student> findAll() {
        return repository.values();
    }


    @Override
    public Student save(Student entity) throws ValidationException {
        nullValidator.validate(entity);
        studentValidator.validate(entity);
        String id = entity.getId();
        if (repository.get(id) == null) {
            repository.put(id, entity);
            return null;
        }
        return entity;
    }


    @Override
    public Student delete(String id) {
        nullValidator.validate(id);
        if (repository.get(id) == null) {
            return null;
        } else {
            return repository.remove(id);
        }
    }


    @Override
    public Student update(Student entity) throws ValidationException {
        nullValidator.validate(entity);
        studentValidator.validate(entity);
        String id = entity.getId();
        if (repository.get(id) != null) {
            repository.put(id, entity);
            return null;
        }
        return entity;
    }


}
