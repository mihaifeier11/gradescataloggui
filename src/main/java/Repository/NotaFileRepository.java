package main.java.Repository;

import javafx.util.Pair;
import main.java.Domain.Student;
import main.java.Domain.Tema;
import main.java.Validator.IAEValidator;
import main.java.Validator.NullValidator;
import main.java.Domain.Nota;
import main.java.Exceptions.ValidationException;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class NotaFileRepository implements CrudRepository<String, Nota> {
    IAEValidator nullValidator = new NullValidator();
    StudentFileRepository studentRepo = new StudentFileRepository();
    TemaFileRepository temaRepo = new TemaFileRepository();

    @Override
    public Nota findOne(String s) {
        return null;
    }

    @Override
    public Iterable<Nota> findAll() {
        List<Nota> allNote = new ArrayList<>();
        Nota tempNota;
        try {
            File fXmlFile = new File("src/main/java/Repository/nota.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);


            NodeList nList = doc.getElementsByTagName("nota");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);


                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    Pair<String, String> id;
                    String idS = eElement.getAttribute("id");
                    id = new Pair<>(idS.split("=")[0], idS.split("=")[1]);
                    Integer valoare = Integer.parseInt(eElement.getElementsByTagName("valoare").item(0).getTextContent());
                    Integer data = Integer.parseInt(eElement.getElementsByTagName("data").item(0).getTextContent());
                    Integer deadline = Integer.parseInt(eElement.getElementsByTagName("deadline").item(0).getTextContent());
                    String feedback = eElement.getElementsByTagName("feedback").item(0).getTextContent();

                    Student st = (Student) studentRepo.findOne(id.getKey());
                    Tema hw = (Tema) temaRepo.findOne(id.getValue());

                    tempNota = new Nota(id, st, hw, valoare, data, deadline, feedback);
                    allNote.add(tempNota);
                }
            }
            return allNote;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Nota save(Nota entity) {
        nullValidator.validate(entity);
        String pathname = "src/main/java/Repository/note/" + entity.getStudent().getId() + ".txt";
        File file = new File(pathname);
        try {
            saveNota(entity);
            FileWriter fr = new FileWriter(file, true);

            fr.write(entity.toString());
            fr.close();


        } catch (IOException e) {
        }
        return null;
    }

    public void saveNota(Nota entity) {
        nullValidator.validate(entity);
        System.out.println(entity);
        List<Nota> allNote = new ArrayList();
        allNote = (List<Nota>) findAll();
        String xmlFilePath = new String("src/main/java/Repository/nota.xml");
        try {

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

            Document document = documentBuilder.newDocument();

            // root element
            Element root = document.createElement("note");
            document.appendChild(root);

            Element nota = document.createElement("nota");

            for (Nota n : allNote) {
                root.appendChild(nota);

                Attr attr = document.createAttribute("id");
                attr.setValue(n.getId().toString());
                nota.setAttributeNode(attr);

                Element valoare = document.createElement("valoare");
                valoare.appendChild(document.createTextNode(Integer.toString(n.getValoare())));
                nota.appendChild(valoare);

                Element data = document.createElement("data");
                data.appendChild(document.createTextNode(Integer.toString(n.getData())));
                nota.appendChild(data);

                Element deadline = document.createElement("deadline");
                deadline.appendChild(document.createTextNode(Integer.toString(n.getDeadline())));
                nota.appendChild(deadline);

                Element feedback = document.createElement("feedback");
                feedback.appendChild(document.createTextNode(n.getFeedback()));
                nota.appendChild(feedback);

                nota = document.createElement("nota");
            }

            Nota n = entity;

            root.appendChild(nota);

            Attr attr = document.createAttribute("id");
            attr.setValue(n.getId().toString());
            nota.setAttributeNode(attr);

            Element valoare = document.createElement("valoare");
            valoare.appendChild(document.createTextNode(Integer.toString(n.getValoare())));
            nota.appendChild(valoare);

            Element data = document.createElement("data");
            data.appendChild(document.createTextNode(Integer.toString(n.getData())));
            nota.appendChild(data);

            Element deadline = document.createElement("deadline");
            deadline.appendChild(document.createTextNode(Integer.toString(n.getDeadline())));
            nota.appendChild(deadline);

            Element feedback = document.createElement("feedback");
            feedback.appendChild(document.createTextNode(n.getFeedback()));
            nota.appendChild(feedback);


            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    @Override
    public Nota delete(String s) {
        return null;
    }

    @Override
    public Nota update(Nota entity) {
        return null;
    }
}
