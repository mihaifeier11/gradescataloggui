package main.java.Domain;

public interface HasID<ID> {
    ID getId();

    void setId(ID id);
}