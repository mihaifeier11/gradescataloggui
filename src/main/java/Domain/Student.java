package main.java.Domain;

import java.util.Objects;

public class Student implements HasID<String> {
    private String id;
    private String nume;
    private int grupa;
    private String email;
    private String cadruIndrumator;

    /**
     * the function creates a student
     *
     * @param id              the id of the student
     *                        id must be String
     * @param nume            the name of the student
     *                        nume must be String
     * @param grupa           the group of the student
     *                        group must be int
     * @param email           the email of the student
     *                        email must be String
     * @param cadruIndrumator the teacher of the student
     *                        cadruIndrumator must be String
     */
    public Student(String id, String nume, int grupa, String email, String cadruIndrumator) {
        this.id = id;
        this.nume = nume;
        this.grupa = grupa;
        this.email = email;
        this.cadruIndrumator = cadruIndrumator;
    }

    /**
     * @return id - the id of the student
     */
    public String getId() {
        return id;
    }

    /**
     * the function sets the id of the student to the one given
     *
     * @param id the new id of the student
     *           must be String
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return nume - the name of the student
     */
    public String getNume() {
        return nume;
    }

    /**
     * the function sets the name of the student to the one given
     *
     * @param nume the new name of the student
     *             must be String
     */
    public void setNume(String nume) {
        this.nume = nume;
    }

    /**
     * @return grupa - the group of the student
     */
    public int getGrupa() {
        return grupa;
    }

    /**
     * the function sets the group of the student to the one given
     *
     * @param grupa the new group of the student
     *              must be int
     */
    public void setGrupa(int grupa) {
        this.grupa = grupa;
    }

    /**
     * @return email - the email of the student
     */
    public String getEmail() {
        return email;
    }

    /**
     * the function sets the email of the student to the one given
     *
     * @param email the new email of the student
     *              must be String
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return cadruIndrumator - the teacher of the student
     */
    public String getCadruIndrumator() {
        return cadruIndrumator;
    }

    /**
     * the function sets the teacher of the student to the one given
     *
     * @param cadruIndrumator the new teacher of the student
     *                        must be String
     */
    public void setCadruIndrumator(String cadruIndrumator) {
        this.cadruIndrumator = cadruIndrumator;
    }

    @Override
    public String toString() {
        return id +
                ", " + nume +
                ", " + grupa +
                ", " + email +
                ", " + cadruIndrumator +
                "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(id, student.id) &&
                grupa == student.grupa &&
                Objects.equals(nume, student.nume) &&
                Objects.equals(email, student.email) &&
                Objects.equals(cadruIndrumator, student.cadruIndrumator);
    }

}
