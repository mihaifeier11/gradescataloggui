package main.java.Domain;

import javafx.util.Pair;

import java.util.Objects;

public class Nota {
    private Pair<String, String> id;
    private Student student;
    private Tema tema;
    private Integer valoare;
    private Integer data;
    private String feedback;

    public Integer getDeadline() {
        return deadline;
    }

    public void setDeadline(Integer deadline) {
        this.deadline = deadline;
    }

    private Integer deadline;

    public Nota(Pair<String, String> id, Student student, Tema tema, Integer valoare, Integer data, Integer deadline, String feedback) {
        this.id = id;
        this.student = student;
        this.tema = tema;
        this.valoare = valoare;
        this.data = data;
        this.deadline = deadline;
        this.feedback = feedback;
    }


    public Pair<String, String> getId() {
        return id;
    }

    public void setId(Pair<String, String> id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Tema getTema() {
        return tema;
    }

    public void setTema(Tema tema) {
        this.tema = tema;
    }

    public Integer getValoare() {
        return valoare;
    }

    public void setValoare(Integer valoare) {
        this.valoare = valoare;
    }

    public Integer getData() {
        return data;
    }

    public void setData(Integer data) {
        this.data = data;
    }

    public String getFeedback() {
        return this.feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Nota nota = (Nota) o;
        return Objects.equals(id, nota.id);
    }

    @Override
    public String toString() {
        return "Tema:" + tema.getId() + System.lineSeparator() +
                "Nota:" + valoare + System.lineSeparator() +
                "Predata in saptamana:" + data + System.lineSeparator() +
                "Deadline:" + deadline + System.lineSeparator();
    }

    public String toStringC() {
        return "Id student:" + student.getId() + System.lineSeparator() +
                "Id tema:" + tema.getId() + System.lineSeparator() +
                "Nota:" + valoare + System.lineSeparator() +
                "Predata in saptamana:" + data + System.lineSeparator() +
                "Deadline:" + deadline + System.lineSeparator();
    }


}
